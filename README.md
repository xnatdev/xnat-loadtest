# XNAT data load testing

These scripts can be used to send a large number of imaging sessions to an XNAT
server, testing the ability of the server to generate new subjects and sessions
on the fly.

## Running

1. Make sure your XNAT server is up and running.
1. Create the destination project on your XNAT server with auto-archive enabled.
  The default project is **TEST**, but you can override this with command-line
  options when you actually invoke the script.
1. Run the **sendFiles.sh** script from the folder in which it's stored:
        ./sendFiles.sh

## Options

The **sendFiles.sh** script accepts the following parameters:

* **-host=_HOST_** specifies the address of the XNAT server. Defaults to
  **localhost**.
* **-ae=_AETITLE_** specifies the AE title for the DICOM SCP receiver. Defaults
  to **XNAT**.
* **-port=_PORT_** specifies the port for the DICOM SCP receiver. Defaults to
  **8104**.
* **-project=_PROJECT_** specifies the destination project for incoming DICOM
  sessions. Defaults to **TEST**.
* **-prefix=_PREFIX_** specifies the prefix for generated subject labels.
  Defaults to **test**.
* **-attempts=_N_** specifies the number of attempts to send DICOM sessions to
  the XNAT server. Defaults to unlimited (requires manually terminating test by
  pressing Ctrl-C).
* **-wait=_WAIT_** specifies how long to wait between send attempts. Defaults to
  **20s**.
  
