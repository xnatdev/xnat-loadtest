#!/bin/bash

contains() {
    local VAR
    for VAR in "${@:2}"; do
        [[ "${VAR}" == "${1}" ]] && return 0;
    done
    return 1
}

function help() {
    echo "${0}"
    echo
    echo "Values inside <> indicate the default value used if the option isn't specified."
    echo "Values inside [] indicate there is no default value if the option isn't specified."
    echo
    echo "Options:"
    echo
    echo "    -host=<xnatdev.xnat.org>"
    echo "    -user=<owner>"
    echo "    -password=<owner>"
    echo "    -project=<TEST>"
    echo "    -subjectCount=<400>"
    echo "    -exptsPerSubject=<100>"
    echo "    -xsitype=<xnat:mrSessionData>"
    echo "    -sudo=[user]"
    echo "    -local=<xnat>"
    echo
    echo "The user specifid by the -sudo parameter must have sudo access to /sbin/service"
    echo "on the target server. If -sudo isn't specified, the sudo operations won't be"
    echo "performed."
    exit 0
}

function timestamp() {
    CMD_TO_RUN="${@}"
    START_TIME=$(gdate +%s%N | cut -b1-13)
    echo $(date): ${CMD_TO_RUN}
    eval "${CMD_TO_RUN}"
    echo
    END_TIME=$(gdate +%s%N | cut -b1-13)
    echo Elapsed: $(( END_TIME - START_TIME )) ms
    echo
}

function createSubject() {
    timestamp http --session=${XNAT_USER} --print hb PUT ${HOST}/data/projects/${PROJECT}/subjects/${SUBJECT}
}

function createExperiment() {
    timestamp http --session=${XNAT_USER} --print hb PUT ${HOST}/data/projects/${PROJECT}/subjects/${SUBJECT}/experiments/${EXPERIMENT} xsiType==${XSITYPE}
}

function runTests() {
    timestamp http --auth ${XNAT_USER}:${PASSWORD} --session=${XNAT_USER} ${HOST}/data/JSESSION
    timestamp http --session=${XNAT_USER} --print hb PUT ${HOST}/data/projects/${PROJECT}

    for CURRENT_SUBJECT_INDEX in $(seq -f "%05g" 1 ${SUBJECT_COUNT}); do
        SUBJECT="${PROJECT}_${CURRENT_SUBJECT_INDEX}"
        createSubject
        for CURRENT_EXPT_INDEX in $(seq -f "%05g" 1 ${EXPTS_PER_SUBJECT}); do
            EXPERIMENT="${SUBJECT}_${CURRENT_EXPT_INDEX}"
            createExperiment
        done
    done
}

function doAsSudo() {
    if [[ ! -z ${SUDO} ]]; then
        ssh ${SUDO}@${HOST} sudo $@
    fi
}

function doAsLocal() {
    if [[ ! -z ${SUDO} ]]; then
        ssh ${LOCAL}@${HOST} $@
    fi
}

contains "-help" $@

if [[ $? == 0 ]]; then
    help
fi

while [[ ! -z ${1} ]]; do
    if [[ "${1}" =~ ^-host=* ]]; then
        HOST=$(echo ${1} | cut -f 2 -d =)
    elif [[ "${1}" =~ ^-user=* ]]; then
        XNAT_USER=$(echo ${1} | cut -f 2 -d =)
    elif [[ "${1}" =~ ^-password=* ]]; then
        PASSWORD=$(echo ${1} | cut -f 2 -d =)
    elif [[ "${1}" =~ ^-project=* ]]; then
        PROJECT=$(echo ${1} | cut -f 2 -d =)
    elif [[ "${1}" =~ ^-subjectCount=* ]]; then
        SUBJECT_COUNT=$(echo ${1} | cut -f 2 -d =)
    elif [[ "${1}" =~ ^-exptsPerSubject=* ]]; then
        EXPTS_PER_SUBJECT=$(echo ${1} | cut -f 2 -d =)
    elif [[ "${1}" =~ ^-xsitype=* ]]; then
        XSITYPE=$(echo ${1} | cut -f 2 -d =)
    elif [[ "${1}" =~ ^-sudo=* ]]; then
        SUDO=$(echo ${1} | cut -f 2 -d =)
    elif [[ "${1}" =~ ^-local=* ]]; then
        LOCAL=$(echo ${1} | cut -f 2 -d =)
    else
        echo
        echo "I don't recognize this option: ${1}"
        echo
        help
    fi
    shift
done    

[[ -z ${HOST} ]] && { HOST=xnatdev.xnat.org; }
[[ -z ${XNAT_USER} ]] && { XNAT_USER=owner; }
[[ -z ${PASSWORD} ]] && { PASSWORD=owner; }
[[ -z ${PROJECT} ]] && { PROJECT=TEST; }
[[ -z ${SUBJECT_COUNT} ]] && { SUBJECT_COUNT=400; }
[[ -z ${EXPTS_PER_SUBJECT} ]] && { EXPTS_PER_SUBJECT=100; }
[[ -z ${XSITYPE} ]] && { XSITYPE=xnat:mrSessionData; }
[[ -z ${SUDO} ]] && { unset LOCAL; }

TEST_RUN_TIMESTAMP=$(date +%s)
TEST_RUN_OUTPUT_DIR="test-run-${TEST_RUN_TIMESTAMP}"
mkdir ${TEST_RUN_OUTPUT_DIR}
LOG_FILE="${TEST_RUN_OUTPUT_DIR}/${0%.*}-${TEST_RUN_TIMESTAMP}.log"
echo "#!/bin/bash" > watch-this-run.sh
echo "watch -d bash monitorCreateExperiments.sh ${LOG_FILE}" >> watch-this-run.sh
chmod +x watch-this-run.sh
echo Beginning test run. Results can be found in the folder ${TEST_RUN_OUTPUT_DIR}. You can get a
echo running tally of the current state of the test-run by running the watch-this-run.sh script.

exec > >(tee -i ${LOG_FILE})
exec 2>&1

if [[ ! -z ${SUDO} ]]; then
    # echo "Shutting down services in preparation for log captures"
    # doAsSudo /sbin/service tomcat7 stop
    # doAsSudo /sbin/service httpd stop
    # doAsLocal 'find /var/log/tomcat7 ~'"${LOCAL}"'/logs -mindepth 1 -maxdepth 1 -type f -exec rm ''{}'' \;'
    scp git* get-logs.sh ${SUDO}@${HOST}:/tmp
    doAsSudo chmod +x /tmp/git-*.sh /tmp/get-logs.sh
    doAsSudo /tmp/git-init-var-log.sh ${TEST_RUN_TIMESTAMP}
    # scp prefs-init.ini ${LOCAL}@${HOST}:~${LOCAL}/config
    # doAsLocal dropdb ${LOCAL}
    # doAsLocal createdb ${LOCAL}
    # doAsLocal rm -rf /data/xnat/archive/* /data/xnat/prearchive/* /data/xnat/cache/* /data/xnat/build/*
    # doAsSudo /sbin/service httpd start
    # doAsSudo /sbin/service tomcat7 start
fi

runTests

echo "Tests are completed. You can view the results in ${LOG_FILE}."
if [[ ! -z ${SUDO} ]]; then
    doAsSudo /tmp/git-commit-var-log.sh ${TEST_RUN_TIMESTAMP}
    scp "${SUDO}@${HOST}:/tmp/test-run-var-log-diff-*.diff" ${TEST_RUN_OUTPUT_DIR}
    doAsSudo "rm /tmp/test-run-var-log-diff-*.diff"
    doAsLocal /tmp/get-logs.sh ${TEST_RUN_TIMESTAMP}
    scp ${LOCAL}@${HOST}:/tmp/logs-${TEST_RUN_TIMESTAMP}.zip ${TEST_RUN_OUTPUT_DIR}
    doAsSudo rm /tmp/git-* /tmp/get-logs.sh
    echo "The server logs from this test run have been zipped and placed in ~${LOCAL}/logs-${TEST_RUN_TIMESTAMP}.zip on the server,"
    echo "and downloaded into the ${TEST_RUN_OUTPUT_DIR} folder."
fi
