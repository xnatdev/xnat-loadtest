#!/bin/bash

contains() {
    local VAR
    for VAR in "${@:2}"; do
        [[ "${VAR}" == "${1}" ]] && return 0;
    done
    return 1
}

contains "-help" $@

if [[ $? == 0 ]]; then
    echo "sendFiles.sh"
    echo
    echo "Options:"
    echo " -host=HOST"
    echo " -user=USER"
    echo " -password=PASSWORD"
    echo " -stem=STEM"
    echo " -attempts=N"
    exit 0
fi

while [[ ! -z ${1} ]]; do
    if [[ "${1}" =~ ^-host=* ]]; then
        HOST=$(echo ${1} | cut -f 2 -d =)
    elif [[ "${1}" =~ ^-user=* ]]; then
        USER=$(echo ${1} | cut -f 2 -d =)
    elif [[ "${1}" =~ ^-password=* ]]; then
        PASSWORD=$(echo ${1} | cut -f 2 -d =)
    elif [[ "${1}" =~ ^-stem=* ]]; then
        STEM=$(echo ${1} | cut -f 2 -d =)
    elif [[ "${1}" =~ ^-attempts=* ]]; then
        ATTEMPTS=$(echo ${1} | cut -f 2 -d =)
    else
        echo I don''t recognize this option: ${1}
    fi
    shift
done    

[[ -z ${HOST} ]] && { HOST=xnatdev.xnat.org; }
[[ -z ${USER} ]] && { USER=admin; }
[[ -z ${PASSWORD} ]] && { PASSWORD=admin; }
[[ -z ${STEM} ]] && { STEM=TEST; }
[[ -z ${ATTEMPTS} ]] && { ATTEMPTS=30; }

http --auth ${USER}:${PASSWORD} --session=${USER} ${HOST}/data/projects
for INDEX in $(seq -f "%02g" 1 ${ATTEMPTS}); do
    PROJECT=${STEM}_${INDEX}
    echo Creating ${PROJECT}
    http --session=${USER} PUT ${HOST}/data/projects/${PROJECT}
done

