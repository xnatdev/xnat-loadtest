#!/bin/bash
TEST_RUN_TIMESTAMP=${1}

cd /data/xnatdev/home/logs

find /var/log/tomcat7 . -mindepth 1 -maxdepth 1 -type f | xargs zip /tmp/logs-${TEST_RUN_TIMESTAMP}.zip

