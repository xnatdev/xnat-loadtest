#!/bin/bash
cd /var/log
git add .
git commit --quiet --no-status -m "Commit for completion of performance test run ${1}"
DIFF_FILE=/tmp/test-run-var-log-diff-${1}.diff
git format-patch --stdout -1 HEAD > ${DIFF_FILE}
chmod 666 ${DIFF_FILE}
