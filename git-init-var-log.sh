#!/bin/bash
cd /var/log
[[ -d .git ]] && { rm -rf .git; }
git init
git config --global user.name "Test User"
git config --global user.email "jrherrick@wustl.edu"
cp /tmp/gitignore .gitignore
git add .
git commit --quiet --no-status -m "Initial commit for performance test run ${1}"
