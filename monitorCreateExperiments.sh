#!/bin/bash
LOG_FILE=${1}
grep "^Elapsed:" ${LOG_FILE} | sed 's/Elapsed: \([0-9]\{2,12\}\) ms/\1/' > .times

EXPT_COUNT=$(grep "^XNAT_E" ${LOG_FILE} | wc -l)
TIMES_COUNT=$(cat .times | wc -l)
AVG_AND_VAR=$(awk -f average-and-variance.awk .times)
AVG_TIME=$(echo $AVG_AND_VAR | cut -f 1 -d " ")
STD_DEV=$(echo $AVG_AND_VAR | cut -f 2 -d " ")

echo Created ${EXPT_COUNT} experiments with an average time required of ${AVG_TIME} ms and standard deviation of ${STD_DEV}
