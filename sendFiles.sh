#!/bin/bash

contains() {
    local VAR
    for VAR in "${@:2}"; do
        [[ "${VAR}" == "${1}" ]] && return 0;
    done
    return 1
}

contains "-help" $@

if [[ $? == 0 ]]; then
    echo "sendFiles.sh"
    echo
    echo "Options:"
    echo " -host=HOST"
    echo " -ae=AETITLE"
    echo " -port=PORT"
    echo " -project=PROJECT"
    echo " -prefix=PREFIX"
    echo " -attempts=N"
    echo " -wait=WAIT"
    exit 0
fi

while [[ ! -z ${1} ]]; do
    if [[ "${1}" =~ ^-host=* ]]; then
        HOST=$(echo ${1} | cut -f 2 -d =)
    elif [[ "${1}" =~ ^-ae=* ]]; then
        AE=$(echo ${1} | cut -f 2 -d =)
    elif [[ "${1}" =~ ^-port=* ]]; then
        PORT=$(echo ${1} | cut -f 2 -d =)
    elif [[ "${1}" =~ ^-project=* ]]; then
        PROJECT=$(echo ${1} | cut -f 2 -d =)
    elif [[ "${1}" =~ ^-prefix=* ]]; then
        PREFIX=$(echo ${1} | cut -f 2 -d =)
    elif [[ "${1}" =~ ^-attempts=* ]]; then
        ATTEMPTS=$(echo ${1} | cut -f 2 -d =)
    elif [[ "${1}" =~ ^-wait=* ]]; then
        WAIT=$(echo ${1} | cut -f 2 -d =)
    else
        echo I don''t recognize this option: ${1}
    fi
    shift
done    

[[ -z ${HOST} ]] && { HOST=localhost; }
[[ -z ${AE} ]] && { AE=XNAT; }
[[ -z ${PORT} ]] && { PORT=8104; }
[[ -z ${PROJECT} ]] && { PROJECT=TEST; }
[[ -z ${PREFIX} ]] && { PREFIX=test; }
[[ -z ${ATTEMPTS} ]] && { ATTEMPTS=-1; }
[[ -z ${WAIT} ]] && { WAIT=20s; }

echo Starting test sends to the project ${PROJECT} on ${HOST} at AE ${AE}:${PORT}.

DICOM=./dicom
TMPDIR=./tmp

rm -rf $TMPDIR
mkdir $TMPDIR

let COUNT=0
while true
do
    printf -v SUBJECT "${PREFIX}%05d" $COUNT
    echo "Send #${COUNT} subject ${SUBJECT}"
    cp -f $DICOM/* $TMPDIR
    for FILE in $TMPDIR/*.dcm
    do
        dcmodify -m "0010,4000=Project:${PROJECT} Subject:${SUBJECT} Session:${SUBJECT}_1" ${FILE}
        dcmsend -aec ${AE} ${HOST} ${PORT} ${FILE}
    done
    rm -rf ${TMPDIR}/*
    let COUNT=${COUNT}+1
    if (( ${ATTEMPTS} > 0 && ${COUNT} >= ${ATTEMPTS})); then
        break
    fi
    sleep ${WAIT}
done
